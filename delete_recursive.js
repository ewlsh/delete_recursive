const { Gio } = imports.gi;

const getPrototype = (prototype, names) => {
    const p = {};

    names.forEach(method => {
        let async = typeof method === 'string' ? method : method[0];
        let finish = typeof method === 'string' ? `${async.replace(/_async$/, '_finish')}` : method[1];

        p[async] = prototype[async];
        p[finish] = prototype[finish];

        Gio._promisify(p, async, finish);
    });

    return file => Object.assign(file, p);
}

const getAsyncFileEnumeratorMethods = () => {
    try {
        const _LocalFileEnumeratorPrototype = Gio.File
            .new_for_path("/")
            .enumerate_children("", Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null)
            .constructor.prototype;

        return getPrototype(_LocalFileEnumeratorPrototype, ["next_files_async"]);
    } catch (error) {
        log("Failed to get LocalFileEnumeratorPrototype!");
        log(error);
    }
}

const getAsyncFileMethods = () => {
    const methods = ["create_async", "enumerate_children_async", "delete_async", "query_info_async"];

    return getPrototype(Gio._LocalFilePrototype, methods);
}

let promisifyFile = getAsyncFileMethods();
let promisifyFileEnumerator = getAsyncFileEnumeratorMethods();

async function* _iteratorFromFileEnumerator(iter) {
    let more = true;

    while (!iter.is_closed() && more) {
        const files = await iter.next_files_async(10, 0, null);
        more = files.length !== 0;

        yield files;
    }
}

async function iterate_children_async(dir, attributes, flags, priority = 0, cancellable = null) {
    const enumerator = await dir.enumerate_children_async(attributes, flags, priority, cancellable);
    promisifyFileEnumerator(enumerator);

    return _iteratorFromFileEnumerator(enumerator);
}

async function delete_recursive_async(dir, delete_root = false) {
    promisifyFile(dir);

    try {
        const batches = await iterate_children_async(dir, "*", Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, 0, null);

        for await (const files of batches) {
            for (const file of files) {
                if (file.get_file_type() === Gio.FileType.DIRECTORY) {
                    const subdir = dir.get_child(file.get_name());

                    await delete_recursive_async(subdir);

                    await subdir.delete_async(0, null);
                } else {
                    const subfile = promisifyFile(dir.get_child(file.get_name()));

                    await subfile.delete_async(0, null);
                }
            }
        }

        if (delete_root) {
            await dir.delete_async(0, null);
        }
    } catch (err) {
        if (err.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_DIRECTORY)) {
            await dir.delete_async(0, null);
        }

        throw err;
    }
}